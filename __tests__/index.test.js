const { toMatchImageSnapshot } = require("jest-image-snapshot");
const puppeteer = require("puppeteer");
expect.extend({ toMatchImageSnapshot });

beforeAll(async () => {
  browser = await puppeteer.launch();
});

it("renders correctly", async () => {
  const page = await browser.newPage();
  // change below url to whatever you want to snapshot
  await page.goto("http://192.168.0.27:8080/");
  const image = await page.screenshot();
  expect(image).toMatchImageSnapshot();
});

afterAll(async () => {
  await browser.close();
});
