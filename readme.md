# TESTS

## Puppeteer

[Puppeteer](https://pptr.dev/) is the main module which takes screenshots. They have an [extensive API](https://pptr.dev/#?product=Puppeteer&version=v2.0.0&show=api-class-page) so you add/extend your tests to different breakpoints/pages. 👍🏼

## Git hooks

Combine the test before any production git pushes then you have further restriction of pushing a bad commit. [Husky](https://www.npmjs.com/package/husky) seems to be the gold standard.

## Coverage

You don't need 100% coverage, just cover the basics, spin, story etc.
